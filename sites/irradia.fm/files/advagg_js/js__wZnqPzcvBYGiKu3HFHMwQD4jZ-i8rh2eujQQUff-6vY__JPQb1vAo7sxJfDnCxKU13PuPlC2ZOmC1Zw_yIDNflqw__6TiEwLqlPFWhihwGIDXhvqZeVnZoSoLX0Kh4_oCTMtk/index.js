/* Source and licensing information for the line(s) below can be found at http://irradia.fm/es/modules/o_contrib_seven/esi/js/esi.js. */

(function($){Drupal.behaviors.esi={attach:function(context,settings){var contextualize_urls=(typeof Drupal.settings.ESI!=='undefined')&&Drupal.settings.ESI.contextualize_URLs;if(contextualize_urls){$().esiTags().DrupalESIContextualizeURL().handleESI();}
else{$().esiTags().handleESI();}}};$.fn.DrupalESIContextualizeURL=function(){var esi_context_regexp=/\/CACHE%3D([^\/]+)$/;$(this).each(function(){src=$(this).attr('src');if(match=$(this).attr('src').match(esi_context_regexp)){context_key=match[1];context_val=esi_get_user_context(context_key);new_src=src.replace(esi_context_regexp,'/CACHE='+context_val);$(this).attr('src',new_src);}});return this;}})(jQuery);function esi_get_user_context(context_key){cookie_name=Drupal.settings.ESI.cookie_prefix+context_key+Drupal.settings.ESI.cookie_suffix;return esi_get_value_from_cookie(cookie_name);}
function esi_get_value_from_cookie(cookie_name){var all_cookies=document.cookie.split(';');for(var i=0;i<all_cookies.length;i++){if(all_cookies[i].indexOf(cookie_name)===1){var values=all_cookies[i].split('=');return values[1];}}
return'';};;
/* Source and licensing information for the above line(s) can be found at http://irradia.fm/es/modules/o_contrib_seven/esi/js/esi.js. */
/* Source and licensing information for the line(s) below can be found at http://irradia.fm/es/modules/o_contrib_seven/esi/js/jquery.esi.js. */

(function($){$.fn.esiTags=function(){esi_tags=$();if(this.length==0){collection=$('html');}
else{collection=this;}
collection.each(function(){base_element=$(this).get(0);jQuery.each(base_element.getElementsByTagName('esi:include'),function(i,val){if($(this).children().length>0){children=$(this).children().detach();$(this).after(children);}
esi_tags=esi_tags.add($(this));});jQuery.each(base_element.getElementsByTagName('esi:remove'),function(i,val){esi_tags=esi_tags.add($(this));});});return esi_tags;};$.fn.handleESIChildren=function(){$(this).each(function(){});return this;}
$.fn.handleESI=function(){$(this).each(function(){switch(this.nodeName.toLowerCase()){case'esi:include':$(this).handleESIInclude();break;case'esi:remove':$(this).handleESIRemove();break;}});return this;}
$.fn.handleESIInclude=function(){src=$(this).attr('src');jQuery.ajax({context:this,url:src,success:function(data,textStatus,jqXHR){esiElement=$(this);esiElement.replaceWith(data);}});}
$.fn.handleESIRemove=function(){$(this).replaceWith('');}})(jQuery);;;
/* Source and licensing information for the above line(s) can be found at http://irradia.fm/es/modules/o_contrib_seven/esi/js/jquery.esi.js. */
